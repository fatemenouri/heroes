import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Hero } from 'src/app/model/hero.model';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  @Input('heros') herolist :Hero[] |undefined;
  @Output('detailClick') heroDetailClick = new EventEmitter<Hero>();
  constructor() { }

  ngOnInit(): void {
  }
  showHeroDetail(hero: Hero) {
    this.heroDetailClick.emit(hero);
  }
  
}
