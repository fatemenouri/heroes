import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Hero } from 'src/app/model/hero.model';

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.scss']
})
export class HeroDetailsComponent implements OnInit {
  @Input('hero') selectedHero: Hero | undefined;
  @Output('updateHero') updateHeroEvent: EventEmitter<Hero> = new EventEmitter<Hero>();
  constructor() { 
    console.log("hero-details constructor");
  }

  ngOnInit(): void {
    console.log("hero-details oninit");
  }
  updateHero() {
    console.log("hero-details update hero emit(output)");
    this.updateHeroEvent.emit(this.selectedHero);
  }

}
