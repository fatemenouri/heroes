import { Component, Input, OnInit ,OnChanges} from '@angular/core';
import { Hero, HeroList } from 'src/app/model/hero.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit , OnChanges{
  @Input('heros') heroList: HeroList | undefined;

  canViewTopHeros:boolean |undefined;
  canViewHeroList:boolean |undefined;
  canViewHeroDetail:boolean |undefined;
  topHeros : Hero[] |undefined;
  selectedHero: Hero | undefined;
  heroes : Hero[] |undefined;

  constructor() { console.log("dashboard constructor"); }

  ngOnInit(): void {
    console.log("dashboard oninit");
    this.showDashboard();
  }
  
  ngOnChanges() : void{
    console.log("dashboard onchange");
    this.topHeros = this.heroList?.results.slice(0,5);
    this.heroes=this.heroList?.results;
  }

  showDashboard(): void{
    console.log(" show dashboard");
    this.canViewTopHeros=true;
    this.canViewHeroList=false;
    this.canViewHeroDetail=false;
  }
  showHeroList(): void{
    console.log("dashboard show hero list");
    this.canViewTopHeros=false;
    this.canViewHeroList=true;
    this.canViewHeroDetail=false;
  }
  showHeroDetail(): void{
    console.log("dashboard show details");
    this.canViewTopHeros=false;
    this.canViewHeroList=false;
    this.canViewHeroDetail=true;
  }
  onHeroClick(hero: Hero) {
    console.log("dashboard on hero click");
    this.showHeroDetail();
    this.selectedHero = {...hero};
  }

  onHeroUpdate(hero: Hero) {
    console.log("dashboard on hero update ");
    const selected = this.heroList?.results.find(item => item.id == hero.id);
    if(selected) {
      selected.name = hero.name;
    } else {
      alert('Hero NOT FOUND!!!!')
    }
    this.showDashboard();
  }


}
