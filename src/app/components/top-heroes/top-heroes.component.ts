import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Hero } from 'src/app/model/hero.model';

@Component({
  selector: 'app-top-heroes',
  templateUrl: './top-heroes.component.html',
  styleUrls: ['./top-heroes.component.scss']
})
export class TopHeroesComponent implements OnInit {
  @Input('heros') herolist: Hero[] | undefined;
  @Output('detailClick') heroDetailClick = new EventEmitter<Hero>();
  constructor() {
    console.log("tpo-heros constructor");
  }

  ngOnInit(): void {
    console.log("top-heros oninit");
  }

  showHeroDetail(hero: Hero) {
    console.log("top-heros show hero details emit(output)");
    this.heroDetailClick.emit(hero);
  }

}
